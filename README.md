# READ ME

## Usage:

Requires an environment with python modules access:

export PATH="/home/adam/anaconda2/bin:$PATH"

source activate NK

### Sample command

xsltproc -o nk_simulation.py NK_SML_OO.xsl TestProjects/ValueTypes/experiment0.xml; python nk_simulation.py 

Often rebuilding and re-compiling the project is useful to ensure any updates are noticed, a useful way to do this with one command has been:

From the /translator_files/translator folder:

cd ../..; python setup.py install; cd translator_files/translator; xsltproc -o nk_simulation.py NK_SML_OO.xsl  ../models/TestProjects/ValueTypes/experiment0.xml; python nk_simulation.py

This will rebuild, move you back to the translator dir, then translate a document, and attempt to run it.

# Known Issues

This project is still developing and taking shape, as such there will be many untested functions.

## Known Errors:

- Normal Distribution returns zero
- Conductance Based Synapses REQUIRE the main state variable V

## Limitations

- SpineML allows for an arbitary number of regimes, currently we only support a single regime. This is a feature we will add in time.

- Only a single output port is allowed at presen, to conform to the Neurokernel design patterns

- Only a single input port is supported at present

- Only synapse models already in NK are supported, future releases will allow to specify synaptic components


## Work in progress

- Separate out nk_spineml into two files
-- One Concerning the SpineML classes along with nk_component
-- One concerning translating spineML into NK code.

- Expand to allow non-spiking neurons
-- Standard Neurons requires detection of spiking or gpot output
-- based on the send port?
- Expand on LPU construction
-- Currently all neurons are initialised with extern': True,'public': True,'spiking': True
- Seperate out the NK package, from the LibSpineML package
-- Enable re-generation of SpineML xml from the python package

### Spine ML compatability

- Configure Script to run from SpineML directly
-- Add Lesion and Configuration options
-- Add the ability to record specific values
--- Do dynamically based on the output in spineML experiment  

### NK compatability

Currently we create the network as a Single LPU in NK, this could be improved to allow mutiple LPUs
- Allow dynamic Synapse Creation
-- Currently Hard Coded synapse sumation in the Neuron, catering for Conductance and Non-Conductance Based Synapses, Conductance based requires V as the main state variable

## Performance Improvement
- reduce global memory reads by encoding parameter values directly in the C
