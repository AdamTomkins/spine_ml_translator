#!/usr/bin/env python

import setuptools
from distutils.core import setup

setup(name='neurokernel-spineml-translator',
      version='1.0',
      packages=['translator_files',
                'translator_files.neurons',
                'translator_files.synapses',
		        'translator_files.utils',
		        'translator_files.translator',
                'translator_files.models'],

      install_requires=[
        'neurokernel >= 0.1',
      ]
     )
